# AndroidChat

# Android Firebase Chat

Android Firebase Chat based on the following videos:

PT 1: https://www.youtube.com/watch?v=2tRavgYG4ks
PT 2: https://www.youtube.com/watch?v=XyxH6VcMm9E


TABLES  
  
**TABLE -> user**  
id  
first_name  
last_name  
email  
password (hash)  
salt (?)  
date_added  
banned_until  
muted_until  
is_admin  
  
**TABLE -> message**  
id  
text  
user_id  
conversation_id  
timestamp  
delivery_status (enum)  
	- SENDING  
	- SENT  
	- RECEIVED  
	- READ  
  
**TABLE -> conversation** (Easily scalable to chat rooms since all the properties can be applied to both 1 on 1 chats and chat rooms)  
id  
name  
image_url (we can use firebase storage. It allows images and it provides a direct url - one liner to display an image in android and no need to implement cache.)  
user_ids  
message_ids  
  
**TABLE -> user_to_conversation**  
conversation_id  
user_id