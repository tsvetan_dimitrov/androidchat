package com.dets.base;

import android.os.Build;
import android.support.annotation.RequiresApi;

import java.util.Objects;

public class Message {
    private String text;
    private String senderId;
    private String conversationId;
    private long timeStamp;

    public Message() {}

    public Message(String text, String senderId, String conversationId, long timeStamp) {
        this.text = text;
        this.senderId = senderId;
        this.conversationId = conversationId;
        this.timeStamp = timeStamp;
    }

    public String getConversationId() {
        return conversationId;
    }

    public String getText() {
        return text;
    }

    public String getSenderId() {
        return senderId;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return timeStamp == message.timeStamp &&
                Objects.equals(text, message.text) &&
                Objects.equals(senderId, message.senderId);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {

        return Objects.hash(text, senderId, timeStamp);
    }
}
