package com.dets.base;

import com.dets.base.User;

public class SelectableUser extends User {
    private boolean isSelected;

    public SelectableUser(User user, boolean isSelected) {
        super(user.getUid(), user.getUserName(), user.getFirstName(), user.getLastName(), user.getEmail(), user.isAdmin());
        this.isSelected = isSelected;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
