package com.dets.base;

import com.google.firebase.database.Exclude;

import java.util.Locale;
import java.util.Map;

public class User {
    private String uid;
    private String userName;
    private String firstName;
    private String lastName;
    private String email;
    private boolean isAdmin;
    private Map<String, Boolean> conversationIds;

    public User() {}

    public User(String uid, String userName, String firstName, String lastName, String email, boolean isAdmin) {
        this.uid = uid;
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.isAdmin = isAdmin;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public Map<String, Boolean> getConversationIds() {
        return conversationIds;
    }

    public void setConversationIds(Map<String, Boolean> conversations) {
        this.conversationIds = conversations;
    }

    @Override
    public String toString() {
        return String.format(Locale.getDefault(), "%s %s (%s)", firstName, lastName, userName);
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUid() {
        return uid;
    }

    @Exclude
    public boolean isPartOfConversation(String conversationId) {
        return conversationIds != null && conversationIds.keySet().contains(conversationId);
    }
}
