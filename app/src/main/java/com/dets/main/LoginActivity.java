package com.dets.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.dets.main.conversations.list.ConversationListActivity;
import com.dets.tools.ProgressDialogUtilities;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity {
    private EditText mEditEmail;
    private EditText mEditPassword;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle(getResources().getString(R.string.title_login_activity));

        mEditEmail = findViewById(R.id.edit_email);
        mEditPassword = findViewById(R.id.edit_password);
        mAuth = FirebaseAuth.getInstance();
        checkAuthenticated();
    }


    public void registerButtonClicked(View view) {
        Intent registerIntent = new Intent(this, RegisterActivity.class);
        startActivity(registerIntent);
    }

    public void loginButtonClicked(View view) {
        if (!validateForm()) {
            return;
        }

        ProgressDialogUtilities.showProgressDialog(getString(R.string.loading), this);

        mAuth.signInWithEmailAndPassword(mEditEmail.getText().toString(), mEditPassword.getText().toString())
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        Intent chatHomeIntent = new Intent(LoginActivity.this, ConversationListActivity.class);
                        startActivity(chatHomeIntent);
                    } else {
                        Toast.makeText(LoginActivity.this, getString(R.string.authentication_failed),
                                Toast.LENGTH_SHORT).show();
                    }

                    ProgressDialogUtilities.hideProgressDialog();
                });
    }

    private boolean validateForm() {
        boolean valid = true;

        String email = mEditEmail.getText().toString();
        if (TextUtils.isEmpty(email)) {
            mEditEmail.setError(getString(R.string.required));
            valid = false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mEditEmail.setError(getString(R.string.invalid_email));
            valid = false;
        } else {
            mEditEmail.setError(null);
        }

        String password = mEditPassword.getText().toString();
        if (TextUtils.isEmpty(password)) {
            mEditPassword.setError(getString(R.string.required));
            valid = false;
        } else {
            mEditPassword.setError(null);
        }

        return valid;
    }

    private void checkAuthenticated() {
        if (mAuth.getCurrentUser() != null) {
            Intent registerIntent = new Intent(this, ConversationListActivity.class);
            startActivity(registerIntent);
        }
    }
}
