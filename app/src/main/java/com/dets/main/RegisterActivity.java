package com.dets.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.dets.tools.ProgressDialogUtilities;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.dets.base.User;
import com.dets.tools.ImageUtilities;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterActivity extends AppCompatActivity {
    private EditText mEditUserName;
    private EditText mEditFirstName;
    private EditText mEditLastName;
    private EditText mEditPassword;
    private EditText mEditEmail;
    private EditText mEditConfirmPassword;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setTitle(getResources().getString(R.string.title_register_activity));

        mEditUserName = findViewById(R.id.edit_username);
        mEditFirstName = findViewById(R.id.edit_first_name);
        mEditLastName = findViewById(R.id.edit_last_name);
        mEditPassword = findViewById(R.id.edit_password);
        mEditEmail = findViewById(R.id.edit_email);
        mEditConfirmPassword = findViewById(R.id.edit_confirm_password);

        mAuth = FirebaseAuth.getInstance();
    }

    public void registerButtonClicked(View view) {
        createAccount(mEditEmail.getText().toString(), mEditPassword.getText().toString());
    }

    private void createAccount(String email, String password) {
        if (!validateForm()) {
            return;
        }

        ProgressDialogUtilities.showProgressDialog(getString(R.string.loading), this);

        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(
                task -> {
                    if (task.isSuccessful()) {
                        // Write user to db
                        writeNewUser(mAuth.getCurrentUser());

                        // Log user out to initiate login
                        mAuth.signOut();

                        // Redirect to login
                        Intent loginIntent = new Intent(RegisterActivity.this, LoginActivity.class);
                        startActivity(loginIntent);
                    } else {
                        String error = task.getException() == null ? getString(R.string.authentication_failed) : task.getException().getLocalizedMessage();
                        Toast.makeText(RegisterActivity.this, error, Toast.LENGTH_SHORT).show();
                    }

                    ProgressDialogUtilities.hideProgressDialog();
                });
    }

    private boolean validateForm() {
        boolean valid = true;

        String email = mEditEmail.getText().toString();
        if (TextUtils.isEmpty(email)) {
            mEditEmail.setError(getString(R.string.required));
            valid = false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mEditEmail.setError(getString(R.string.invalid_email));
            valid = false;
        } else {
            mEditEmail.setError(null);
        }

        String firstName = mEditFirstName.getText().toString();
        if (TextUtils.isEmpty(firstName)) {
            mEditFirstName.setError(getString(R.string.required));
            valid = false;
        } else {
            mEditFirstName.setError(null);
        }

        String lastName = mEditLastName.getText().toString();
        if (TextUtils.isEmpty(lastName)) {
            mEditLastName.setError(getString(R.string.required));
            valid = false;
        } else {
            mEditLastName.setError(null);
        }

        String userName = mEditUserName.getText().toString();
        if (TextUtils.isEmpty(userName)) {
            mEditUserName.setError(getString(R.string.required));
            valid = false;
        } else {
            mEditUserName.setError(null);
        }

        String password = mEditPassword.getText().toString();
        if (TextUtils.isEmpty(password)) {
            mEditPassword.setError(getString(R.string.required));
            valid = false;
        } else if (password.length() < 6) {
            mEditPassword.setError(getString(R.string.error_password_length));
            valid = false;
        } else {
            mEditPassword.setError(null);
        }

        String confirmPassword = mEditConfirmPassword.getText().toString();
        if (TextUtils.isEmpty(confirmPassword)) {
            mEditConfirmPassword.setError(getString(R.string.required));
            valid = false;
        } else if (!confirmPassword.equals(password)) {
            mEditConfirmPassword.setError(getString(R.string.error_match_password));
            valid = false;
        } else {
            mEditConfirmPassword.setError(null);
        }

        return valid;
    }

    private void writeNewUser(FirebaseUser fbUser) {
        DatabaseReference mDbRef = FirebaseDatabase.getInstance().getReference();

        User user = new User();
        user.setUserName(mEditUserName.getText().toString());
        user.setFirstName(mEditFirstName.getText().toString());
        user.setLastName(mEditLastName.getText().toString());
        user.setEmail(mEditEmail.getText().toString());
        user.setAdmin(false);
        user.setUid(fbUser.getUid());

        mDbRef.child("users").child(user.getUid()).setValue(user);
    }
}
