package com.dets.main;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dets.base.User;
import com.dets.tools.ActivityCodes;
import com.dets.tools.ImageUtilities;
import com.dets.tools.NoImageException;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;

public class ProfileActivity extends AppCompatActivity {
    private ImageView userImage;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        loadUser();
    }

    private void loadUser() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if(bundle != null) {
            String userId = bundle.getString(ActivityCodes.EXTRA_NAME_USER_ID);
            if(userId != null)
                FirebaseDatabase.getInstance().getReference()
                        .child("users")
                        .child(userId)
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if(dataSnapshot.getValue(User.class) != null) {
                                    user = dataSnapshot.getValue(User.class);
                                    fillViewsWithData();
                                }
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
        }
    }

    private void fillViewsWithData() {
        userImage = findViewById(R.id.image_profile_user_image);
        TextView userUsername = findViewById(R.id.text_profile_user_username);
        TextView userFirstName = findViewById(R.id.text_profile_user_first_name);
        TextView userLastName = findViewById(R.id.text_profile_user_last_name);
        TextView userEmail = findViewById(R.id.text_profile_user_email);

        try {
            ImageUtilities.retrieveImageByUrl(userImage, user.getUid());
        } catch (NoImageException e) {
            Log.e("images", e.toString());
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, getString(R.string.error_storage_permission), Toast.LENGTH_SHORT).show();
        }
        userUsername.setText(user.getUserName());
        userFirstName.setText(user.getFirstName());
        userLastName.setText(user.getLastName());
        userEmail.setText(user.getEmail());
    }

    public void userImageClicked(View view) {
        Intent imageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (imageIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(imageIntent, ActivityCodes.REQUEST_TAKE_PICTURE);
        } else {
            Toast.makeText(this, getResources().getString(R.string.error_start_camera), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == ActivityCodes.REQUEST_TAKE_PICTURE) {
            if(data != null && data.getExtras() != null) {
                Bundle extras = data.getExtras();
                Bitmap image = (Bitmap) extras.get("data");
                userImage.setImageBitmap(image);
                try {
                    ImageUtilities.saveImage(this, image, user);
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(this, getString(R.string.error_storage_permission), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, getResources().getString(R.string.error_no_photo_taken), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
