package com.dets.main.conversations.body;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dets.base.Conversation;
import com.dets.base.Message;
import com.dets.main.R;
import com.dets.tools.ImageUtilities;
import com.dets.tools.NoImageException;
import com.dets.tools.TimeUtilities;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;

public class ConversationAdapter extends RecyclerView.Adapter<ConversationViewHolder> {
    private static final int OUTGOING_VIEW_TYPE = 1;
    private static final int INCOMING_VIEW_TYPE = 2;

    private String loggedInUserId;
    private Conversation conversation;
    private DatabaseReference mDbRef;

    ConversationAdapter(String loggedInUserId, Conversation conversation, ChildEventListener listener) {
        this.loggedInUserId = loggedInUserId;
        this.conversation = conversation;
        this.mDbRef = FirebaseDatabase.getInstance().getReference();
        mDbRef.child("messages").orderByChild("conversationId").equalTo(conversation.getId()).addChildEventListener(listener);
    }

    @NonNull
    @Override
    public ConversationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case OUTGOING_VIEW_TYPE:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.conversation_row_outgoing, parent, false);
                break;
            default:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.conversation_row_incoming, parent, false);
        }
        return new ConversationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ConversationViewHolder holder, int position) {
        Message message = conversation.getMessages().get(position);
        Message prevMsg = null;
        holder.getTextConversationBodyMessage().setText(message.getText());
        if(position != 0) { // has previous message
            prevMsg = conversation.getMessages().get(position - 1);
        }
        hideDetailsForMessageChains(holder, prevMsg, message);
        addTimeStamp(holder, message);
    }

    private void hideDetailsForMessageChains(ConversationViewHolder holder, Message prevMsg, Message message) {
        if(!isSameSender(message, prevMsg)) {
            ImageView userPhoto = holder.getImageConversationUserPhoto();
            try {
                ImageUtilities.retrieveImageByUrl(userPhoto, message.getSenderId());
                holder.getImageConversationUserPhoto().setVisibility(View.VISIBLE);
            } catch (IllegalArgumentException | NoImageException | IOException e) {
                // If image does not exist set the default profile image
                e.printStackTrace();
                Log.e("images", e.toString());
                userPhoto.setImageBitmap(ImageUtilities.getDefaultProfileImage(holder.itemView.getContext()));
            }
        } else {
            holder.getImageConversationUserPhoto().setVisibility(View.INVISIBLE);
        }
    }

    private void addTimeStamp(ConversationViewHolder holder, Message message) {
        TextView timestampView = holder.getTextConversationBodyTimestamp();
        String formattedTime = TimeUtilities.getFormattedTimestamp(message.getTimeStamp());
        timestampView.setText(formattedTime);
    }

    private boolean isSameSender(Message message1, Message message2) {
        return message1 != null && message2 != null && message1.getSenderId().equals(message2.getSenderId());
    }

    @Override
    public int getItemCount() {
        return conversation.getMessages().size();
    }

    @Override
    public int getItemViewType(int position) {
        Message messageAtPosition = conversation.getMessages().get(position);
        if(messageAtPosition.getSenderId().equals(loggedInUserId))
            return OUTGOING_VIEW_TYPE;
        return INCOMING_VIEW_TYPE;
    }

}
