package com.dets.main.conversations.add;

import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dets.base.SelectableUser;
import com.dets.main.R;

public class AddConversationViewHolder extends RecyclerView.ViewHolder {
    private ConstraintLayout mLayout;
    private ImageView mUserImage;
    private TextView mUsermame;

    SelectableUser selectableUser;

    AddConversationViewHolder(View itemView) {
        super(itemView);
        mLayout = itemView.findViewById(R.id.layout_add_conversation_row);
        mLayout.setOnClickListener(view -> {
            selectableUser.setSelected(!selectableUser.isSelected());
            if(selectableUser.isSelected())
                mLayout.setBackgroundColor(itemView.getContext().getResources().getColor(R.color.colorPrimaryLight));
            else
                mLayout.setBackgroundColor(Color.TRANSPARENT);
        });
        mUserImage = itemView.findViewById(R.id.image_add_conversation_user_image);
        mUsermame = itemView.findViewById(R.id.text_add_conversation_row_username);
    }

    public ConstraintLayout getLayout() {
        return mLayout;
    }

    public ImageView getUserImage() {
        return mUserImage;
    }

    public TextView getUsermame() {
        return mUsermame;
    }
}
