package com.dets.main.conversations.list;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.dets.base.Conversation;
import com.dets.base.Message;
import com.dets.base.User;
import com.dets.main.R;
import com.dets.main.conversations.body.ConversationActivity;
import com.dets.tools.ImageUtilities;
import com.dets.tools.NoImageException;
import com.dets.tools.TimeUtilities;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConversationListAdapter extends RecyclerView.Adapter<ConversationListViewHolder> {
    private Context mContext;
    private User mUser;
    private List<Conversation> mConversations;
    private FirebaseDatabase db;
    private Map<Conversation, ChildEventListener> mConversationListeners;

    ConversationListAdapter(Context context, User user) {
        this.mContext = context;
        this.mUser = user;
        this.mConversations = new ArrayList<>();
        this.db = FirebaseDatabase.getInstance();
        mConversationListeners = new HashMap<>();
    }

    @NonNull
    @Override
    public ConversationListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.conversation_list_listview_row, parent, false);
        return new ConversationListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ConversationListViewHolder holder, int position) {
        Conversation conversation = mConversations.get(position);
        holder.getConversationName().setText(conversation.getName());
        try {
            ImageUtilities.retrieveImageByUrl(holder.getConversationImage(), ImageUtilities.CONVERSATION_IMAGE_URL);
        } catch (IllegalArgumentException | IOException | NoImageException e) {
            // If image does not exist set the default profile image
            e.printStackTrace();
            Log.e("images", e.toString());
            holder.getConversationImage().setImageBitmap(ImageUtilities.getDefaultProfileImage(holder.itemView.getContext()));
        }
        Message lastMessage = conversation.getLastMessage();
        if(lastMessage != null) {
            holder.getConversationTimestamp().setText(TimeUtilities.getFormattedTimestamp(lastMessage.getTimeStamp()));
            holder.getConversationMessage().setText(lastMessage.getText());
        } else {
            holder.getConversationTimestamp().setText("");
            holder.getConversationMessage().setText("");
        }
        holder.itemView.setOnCreateContextMenuListener((menu, v, menuInfo) -> {
            MenuItem leaveChat = menu.add(R.string.menu_option_leave_chat);
            leaveChat.setOnMenuItemClickListener(item -> leaveConversation(holder.getAdapterPosition()));
            if(mUser.isAdmin()) {
                MenuItem renameChat = menu.add(R.string.menu_option_rename_chat);
                renameChat.setOnMenuItemClickListener(item -> renameConversation(holder.getAdapterPosition()));
                MenuItem deleteChat = menu.add(R.string.menu_option_delete_chat);
                deleteChat.setOnMenuItemClickListener(item -> removeConversation(holder.getAdapterPosition()));
            }
        });
        holder.itemView.setOnClickListener(v -> conversationClicked(holder.getAdapterPosition()));
    }

    private void conversationClicked(int position) {
        Intent conversationIntent = new Intent(mContext, ConversationActivity.class);
        conversationIntent.putExtra("conversationId", mConversations.get(position).getId());
        mContext.startActivity(conversationIntent);
    }

    private boolean leaveConversation(int position) {
        Conversation conversation = mConversations.get(position);
        removeCurrentUserEntryFromConversation(conversation);
        removeMessagesListenerForConversation(conversation);
        return removeConversationFromList(position);
    }

    private void removeCurrentUserEntryFromConversation(Conversation conversation) {
        db.getReference()
                .child("users")
                .child(mUser.getUid())
                .child("conversationIds")
                .child(conversation.getId())
                .removeValue();
    }

    private void removeMessagesListenerForConversation(Conversation conversation) {
        db.getReference()
                .child("messages")
                .orderByChild("conversationId")
                .equalTo(conversation.getId())
                .removeEventListener(mConversationListeners.get(conversation));
        mConversationListeners.remove(conversation);
    }

    private boolean renameConversation(int position) {
        // TODO: Rename conversation in the database entry
        return renameConversationInList(position);
    }

    private boolean removeConversation(int position) {
        // TODO: Remove conversation entry from database
        return removeConversationFromList(position);
    }

    private boolean removeConversationFromList(int position) {
        mConversations.remove(position);
        notifyItemRemoved(position);
        return true;
    }

    private boolean renameConversationInList(int position) {
        Conversation conversation = mConversations.get(position);
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        EditText editText = new EditText(mContext);
        editText.setText(conversation.getName());
        builder.setView(editText);
        builder.setPositiveButton("Rename", (dialog, which) -> {
            conversation.setName(editText.getText().toString());
            notifyItemChanged(position);
        });
        builder.show();
        return true;
    }

    @Override
    public int getItemCount() {
        return mConversations.size();
    }

    public void addConversation(Conversation conversation, ChildEventListener listener) {
        mConversations.add(conversation);
        setLastMessageListener(conversation, listener);
        mConversationListeners.put(conversation, listener);
        notifyDataSetChanged();
    }

    private void setLastMessageListener(Conversation conversation, ChildEventListener listener) {
        FirebaseDatabase.getInstance().getReference().child("messages").orderByChild("conversationId").equalTo(conversation.getId())
                .limitToLast(1).addChildEventListener(listener);
    }

    public void conversationChanged(Conversation conversation) {
        for (int i = 0; i < mConversations.size(); i++)
            if(mConversations.get(i).getId().equals(conversation.getId()))
                notifyItemChanged(i);
    }
}
