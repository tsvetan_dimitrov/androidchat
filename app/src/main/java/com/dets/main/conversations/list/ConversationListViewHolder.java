package com.dets.main.conversations.list;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dets.main.R;

public class ConversationListViewHolder extends RecyclerView.ViewHolder {
    private ImageView mConversationImage;
    private TextView mConversationName;
    private TextView mConversationMessage;
    private TextView mConversationTimestamp;

    ConversationListViewHolder(View view) {
        super(view);
        mConversationImage = view.findViewById(R.id.image_conversation_user_photo);
        mConversationName = view.findViewById(R.id.text_conversation_name);
        mConversationMessage = view.findViewById(R.id.text_conversation_message);
        mConversationTimestamp = view.findViewById(R.id.text_conversation_timestamp);
    }

    public ImageView getConversationImage() {
        return mConversationImage;
    }

    public TextView getConversationName() {
        return mConversationName;
    }

    public TextView getConversationMessage() {
        return mConversationMessage;
    }

    public TextView getConversationTimestamp() {
        return mConversationTimestamp;
    }
}
