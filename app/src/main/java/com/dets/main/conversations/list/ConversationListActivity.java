package com.dets.main.conversations.list;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.dets.base.Conversation;
import com.dets.base.Message;
import com.dets.base.User;
import com.dets.main.ProfileActivity;
import com.dets.main.R;
import com.dets.main.conversations.add.AddConversationActivity;
import com.dets.tools.ActivityCodes;
import com.dets.tools.ProgressDialogUtilities;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ConversationListActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private FirebaseUser fbUser;
    private DatabaseReference mDbRef;
    private User user;
    private ConversationListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation_list);
        mAuth = FirebaseAuth.getInstance();
        fbUser = mAuth.getCurrentUser();
        mDbRef = FirebaseDatabase.getInstance().getReference();
        setSupportActionBar(findViewById(R.id.toolbar_conversation_list));
        fetchUser();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Toolbar tb = findViewById(R.id.toolbar_conversation_list);
        tb.inflateMenu(R.menu.action_bar_menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menu_item_profile) {
            Intent profileIntent = new Intent(this, ProfileActivity.class);
            profileIntent.putExtra(ActivityCodes.EXTRA_NAME_USER_ID, fbUser.getUid());
            startActivity(profileIntent);
            return true;
        } else if(item.getItemId() == R.id.menu_item_add) {
            Intent addIntent = new Intent(this, AddConversationActivity.class);
            addIntent.putExtra(ActivityCodes.EXTRA_NAME_REQUEST_CODE, ActivityCodes.REQUEST_ADD_CONVERSATION);
            addIntent.putExtra(ActivityCodes.EXTRA_NAME_CURRENT_USER_ID, fbUser.getUid());
            startActivity(addIntent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupListView() {
        RecyclerView conversationView = findViewById(R.id.recycler_view_conversations);
        conversationView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        conversationView.setItemAnimator(new DefaultItemAnimator());
        adapter = new ConversationListAdapter(this, user);
        conversationView.setAdapter(adapter);
        registerForContextMenu(conversationView);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Are you sure you want to log out?");
        builder.setPositiveButton("Yes", (dialog, which) -> {
            mAuth.signOut();
            finish();
        });
        builder.setNegativeButton("No", null);
        builder.show();
    }

    private void attachConversationsListener() {
        mDbRef.child("users").child(fbUser.getUid()).child("conversationIds").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                fetchConversation(dataSnapshot.getKey());
            }
            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void fetchUser() {
        // Display dialog for async user fetch
        ProgressDialogUtilities.showProgressDialog(getString(R.string.loading), this);
        mDbRef.child("users").child(fbUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                user = dataSnapshot.getValue(User.class);
                setTitle(String.format("%s's chats", user.getFirstName()));
                setupListView();
                // Hide progress dialog
                ProgressDialogUtilities.hideProgressDialog();
                attachConversationsListener();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void fetchConversation(String key) {
        mDbRef.child("conversations").child(key).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Conversation conversation = dataSnapshot.getValue(Conversation.class);
                if(conversation == null)
                    return;
                adapter.addConversation(conversation, listenForLastMessage(conversation));

                // Hide progress dialog
                ProgressDialogUtilities.hideProgressDialog();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private ChildEventListener listenForLastMessage(Conversation conversation) {
        return new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                List<Message> messages = new ArrayList<>();
                messages.add(dataSnapshot.getValue(Message.class));
                conversation.setMessages(messages);
                adapter.conversationChanged(conversation);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
    }
}
