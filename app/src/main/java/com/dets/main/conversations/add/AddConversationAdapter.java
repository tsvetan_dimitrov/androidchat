package com.dets.main.conversations.add;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dets.base.User;
import com.dets.base.SelectableUser;
import com.dets.main.R;
import com.dets.tools.ImageUtilities;
import com.dets.tools.NoImageException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AddConversationAdapter
        extends RecyclerView.Adapter<AddConversationViewHolder> {
    private List<SelectableUser> selectableUsers;

    public AddConversationAdapter() {
        selectableUsers = new ArrayList<>();
    }

    @NonNull
    @Override
    public AddConversationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.conversation_add_users_row, parent, false);
        return new AddConversationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AddConversationViewHolder holder, int position) {
        SelectableUser selectableUser = selectableUsers.get(position);
        holder.getUsermame().setText(selectableUser.toString());
        try {
            ImageUtilities.retrieveImageByUrl(holder.getUserImage(), selectableUser.getUid());
        } catch (NoImageException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(selectableUser.isSelected())
            holder.getLayout().setBackgroundColor(holder.itemView.getContext().getResources().getColor(R.color.colorPrimaryLight));
        else
            holder.getLayout().setBackgroundColor(Color.TRANSPARENT);
        holder.selectableUser = selectableUser;
    }

    @Override
    public int getItemCount() {
        return selectableUsers.size();
    }

    /**
     * Generates the list from the selectable users. Also the Users are actually SelectedUsers but that
     * should not matter to the calling method.
     * @return the users selected in the recycler view
     */
    public List<User> getSelectedUsers() {
        List<User> selectedUsers = new ArrayList<>();
        for (SelectableUser selectableUser : selectableUsers)
            if(selectableUser.isSelected())
                selectedUsers.add(selectableUser);
        return selectedUsers;
    }

    /**
     * Used by the async call to firebase to fill adapter with users.
     * @param user
     */
    public void addUser(User user) {
        selectableUsers.add(new SelectableUser(user, false));
        notifyItemInserted(selectableUsers.size() - 1);
    }
}
