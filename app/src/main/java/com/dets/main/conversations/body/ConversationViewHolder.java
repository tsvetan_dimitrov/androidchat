package com.dets.main.conversations.body;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dets.main.R;

public class ConversationViewHolder extends RecyclerView.ViewHolder {
    private ImageView mImageConversationUserPhoto;
    private TextView mTextConversationBodyMessage;
    private TextView mTextConversationBodyTimestamp;

    ConversationViewHolder(View itemView) {
        super(itemView);
        mImageConversationUserPhoto = itemView.findViewById(R.id.image_conversation_user_photo);
        mTextConversationBodyMessage = itemView.findViewById(R.id.text_conversation_body_message);
        mTextConversationBodyMessage.setOnClickListener(v -> {
            int timestampVisibility = mTextConversationBodyTimestamp.getVisibility();
            mTextConversationBodyTimestamp.setVisibility(timestampVisibility == View.INVISIBLE ? View.VISIBLE : View.INVISIBLE);
        });
        mTextConversationBodyTimestamp = itemView.findViewById(R.id.text_conversation_body_timestamp);
    }

    public ImageView getImageConversationUserPhoto() {
        return mImageConversationUserPhoto;
    }

    public TextView getTextConversationBodyMessage() {
        return mTextConversationBodyMessage;
    }

    public TextView getTextConversationBodyTimestamp() {
        return mTextConversationBodyTimestamp;
    }

}
