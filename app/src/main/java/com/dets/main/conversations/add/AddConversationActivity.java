package com.dets.main.conversations.add;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dets.base.Conversation;
import com.dets.base.User;
import com.dets.main.R;
import com.dets.tools.ActivityCodes;
import com.dets.tools.ProgressDialogUtilities;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AddConversationActivity extends AppCompatActivity {
    private TextView mTextConversationName;
    private EditText mEditConversationName;
    private Button mButtonSubmit;

    private AddConversationAdapter mAdapter;
    private int mLayoutMode;

    private String conversationId;
    private String currentUserUid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_conversation);
        mTextConversationName = findViewById(R.id.text_add_conversation_name);
        mEditConversationName = findViewById(R.id.edit_add_conversation_name);
        mButtonSubmit = findViewById(R.id.button_add_conversation_submit);
        loadExtras();
        setupLayoutMode();
        setupRecyclerView();
    }

    private void loadExtras() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if(bundle != null) {
            conversationId = bundle.getString(ActivityCodes.EXTRA_NAME_CONVERSATION_ID);
            currentUserUid = bundle.getString(ActivityCodes.EXTRA_NAME_CURRENT_USER_ID);
        }
    }

    private void setupLayoutMode() {
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if(extras != null) {
            mLayoutMode = intent.getIntExtra(ActivityCodes.EXTRA_NAME_REQUEST_CODE, ActivityCodes.REQUEST_ADD_CONVERSATION);
            if(mLayoutMode == ActivityCodes.REQUEST_ADD_CONVERSATION) {
                mTextConversationName.setVisibility(View.VISIBLE);
                mEditConversationName.setVisibility(View.VISIBLE);
                mButtonSubmit.setText(R.string.label_create);
            } else if(mLayoutMode == ActivityCodes.REQUEST_ADD_USER_TO_CONVERSATION) {
                mTextConversationName.setVisibility(View.GONE);
                mEditConversationName.setVisibility(View.GONE);
                mButtonSubmit.setText(R.string.label_add);
            }
        }
    }

    private void setupRecyclerView() {
        RecyclerView mUsersRecyclerView = findViewById(R.id.recycler_view_add_conversation_users);
        mUsersRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mUsersRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new AddConversationAdapter();
        retrieveUsers();
        mUsersRecyclerView.setAdapter(mAdapter);
    }

    private void retrieveUsers() {
        ProgressDialogUtilities.showProgressDialog(getString(R.string.loading), this);
        FirebaseDatabase.getInstance().getReference().child("users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    User user = snapshot.getValue(User.class);
                    if(user == null)
                        continue;
                    if(!user.getUid().equals(currentUserUid) && !user.isPartOfConversation(conversationId))
                        mAdapter.addUser(user);
                }
                ProgressDialogUtilities.hideProgressDialog();
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    public void submitButtonPressed(View view) {
        if(!validateInput())
            return;
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference dbRef = database.getReference();
        if(mLayoutMode == ActivityCodes.REQUEST_ADD_CONVERSATION) {
            Conversation conversation = new Conversation();
            conversation.setName(mEditConversationName.getText().toString());
            DatabaseReference pushRef = dbRef.child("conversations").push();
            conversation.setId(pushRef.getKey());
            pushRef.setValue(conversation);
            for (User user : mAdapter.getSelectedUsers())
                pushToUser(user.getUid(), dbRef, conversation, user.toString());
            pushToUser(currentUserUid, dbRef, conversation, null);
        } else if (mLayoutMode == ActivityCodes.REQUEST_ADD_USER_TO_CONVERSATION) {
            addUserToConversation(dbRef);
        }
        finish();
    }

    private boolean validateInput() {
        if(mLayoutMode == ActivityCodes.REQUEST_ADD_CONVERSATION && mEditConversationName.getText().length() == 0) {
            Toast.makeText(this, getString(R.string.error_conversation_name_empty), Toast.LENGTH_SHORT).show();
            return false;
        } else if(mAdapter.getSelectedUsers().size() == 0) {
            Toast.makeText(this, getString(R.string.error_selected_users_zero), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void addUserToConversation(DatabaseReference dbRef) {
        if (conversationId != null) {
            dbRef.child("conversations")
                    .child(conversationId)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            pushConversation(dbRef, dataSnapshot.getValue(Conversation.class));
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
        }
    }

    private void pushConversation(DatabaseReference dbRef, Conversation conversation) {
        for (User user : mAdapter.getSelectedUsers()) {
            pushToConversations(dbRef, conversation);
            pushToUser(user.getUid(), dbRef, conversation, user.toString());
        }
    }

    private void pushToUser(String uid, DatabaseReference dbRef, Conversation conversation, String userTooltip) {
        DatabaseReference userReference = dbRef
                .child("users")
                .child(uid)
                .child("conversationIds")
                .child(conversation.getId());
        userReference.setValue(true)
                .addOnSuccessListener(aVoid ->  {
                    if(userTooltip != null)
                        Toast.makeText(this, "Added " + userTooltip + " to conversation", Toast.LENGTH_SHORT).show();
                })
                .addOnFailureListener(this::printError);
    }

    private void pushToConversations(DatabaseReference dbRef, Conversation conversation) {
        DatabaseReference conversationReference = dbRef
                .child("conversations")
                .child(conversation.getId())
                .push();
        conversationReference.setValue(conversation).addOnFailureListener(this::printError);
    }

    private void printError(Exception e) {
        e.printStackTrace();
        Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }
}
