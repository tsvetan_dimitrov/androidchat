package com.dets.main.conversations.body;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.dets.base.Conversation;
import com.dets.base.Message;
import com.dets.main.ProfileActivity;
import com.dets.main.R;
import com.dets.main.conversations.add.AddConversationActivity;
import com.dets.tools.ActivityCodes;
import com.dets.tools.ProgressDialogUtilities;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ConversationActivity extends AppCompatActivity {
    private EditText mConversationMessage;

    private RecyclerView conversationView;
    private ConversationAdapter adapter;
    private FirebaseUser fbUser;
    private DatabaseReference mDbRef;
    private Conversation conversation;
    private String conversationId;

    public ConversationActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);
        setSupportActionBar(findViewById(R.id.toolbar_conversation_list));
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        fbUser = mAuth.getCurrentUser();
        mDbRef = FirebaseDatabase.getInstance().getReference();
        mConversationMessage = findViewById(R.id.edit_conversation_message);
        mConversationMessage.setOnFocusChangeListener((v, hasFocus) -> scrollToEnd());
        conversationId = getIntent().getStringExtra("conversationId");

        loadConversation();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Toolbar tb = findViewById(R.id.toolbar_conversation_list);
        tb.inflateMenu(R.menu.action_bar_menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menu_item_profile) {
            Intent profileIntent = new Intent(this, ProfileActivity.class);
            profileIntent.putExtra(ActivityCodes.EXTRA_NAME_USER_ID, fbUser.getUid());
            startActivity(profileIntent);
            return true;
        } else if(item.getItemId() == R.id.menu_item_add) {
            Intent addIntent = new Intent(this, AddConversationActivity.class);
            addIntent.putExtra(ActivityCodes.EXTRA_NAME_REQUEST_CODE, ActivityCodes.REQUEST_ADD_USER_TO_CONVERSATION);
            addIntent.putExtra(ActivityCodes.EXTRA_NAME_CONVERSATION_ID, conversationId);
            addIntent.putExtra(ActivityCodes.EXTRA_NAME_CURRENT_USER_ID, fbUser.getUid());
            startActivity(addIntent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupListView() {
        conversationView = findViewById(R.id.recycler_view_conversation_body);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setStackFromEnd(true);
        conversationView.setLayoutManager(layoutManager);
        conversationView.setItemAnimator(new DefaultItemAnimator());
        createAdapter();
        conversationView.setAdapter(adapter);
        // stupid workaround for stupid android bug
        conversationView.addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> {
            if(bottom < oldBottom)
                conversationView.postDelayed(this::scrollToEnd, 100);
        });
        scrollToEnd();
        registerForContextMenu(conversationView);
    }

    public void sendButtonClicked(View view) {
        if(mConversationMessage.getText().length() != 0) {
            pushMessage();
        }
    }

    private void pushMessage() {
        Message message = new Message(
                mConversationMessage.getText().toString(),
                fbUser.getUid(),
                conversationId,
                System.currentTimeMillis()
        );
        messageAddedToGui();
        DatabaseReference pushRef = mDbRef.child("messages").push();
        pushRef.setValue(message)
                .addOnFailureListener(e -> Toast.makeText(this,
                        "Failed to send message: " + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show());
        // Could add a remove from gui on failure but I see no need now
    }

    private void messageAddedToGui() {
        scrollToEnd();
        mConversationMessage.setText("");
    }

    private void scrollToEnd() {
        if(adapter.getItemCount() != 0)
            conversationView.scrollToPosition(adapter.getItemCount() - 1);
    }

    private void createAdapter() {
        adapter = new ConversationAdapter(fbUser.getUid(), conversation, new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Message message = dataSnapshot.getValue(Message.class);
                conversation.getMessages().add(message);
                adapter.notifyItemInserted(conversation.getMessages().size() - 1);
                scrollToEnd();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void loadConversation() {
        ProgressDialogUtilities.showProgressDialog(getString(R.string.loading), this);
        mDbRef.child("conversations").child(conversationId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // Visualize user-related data
                conversation = dataSnapshot.getValue(Conversation.class);
                fetchMessages(conversationId);
                setTitle(conversation.getName());

                // Hide progress dialog
                ProgressDialogUtilities.hideProgressDialog();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void fetchMessages(String conversationId) {
        mDbRef.child("messages").child(conversationId).orderByChild("timeStamp").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<Message> messages = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren())
                    messages.add(snapshot.getValue(Message.class));
                conversation.setMessages(messages);
                setupListView();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(adapter != null)
            scrollToEnd();
    }
}
