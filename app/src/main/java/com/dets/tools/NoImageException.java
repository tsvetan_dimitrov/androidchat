package com.dets.tools;

/**
 * Specific exception to catch for null imageUrls
 */
public class NoImageException extends Throwable {
    @Override
    public String toString() {
        return "Supplied ImageURL is null!";
    }
}
