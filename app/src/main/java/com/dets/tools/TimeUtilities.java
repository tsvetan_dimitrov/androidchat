package com.dets.tools;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class TimeUtilities {
    private static final String HOUR_FORMAT = "HH:mm";
    private static final String DAY_FORMAT = "EEE";
    private static final String DATE_FORMAT = "dd.MM";
    private static final String YEAR_FORMAT = "dd.MM.yyyy";

    public static String getFormattedTimestamp(long timestamp) {
        String format;
        long diff = System.currentTimeMillis() - timestamp;
        if(diff <= TimeUnit.DAYS.toMillis(1)) {
            format = HOUR_FORMAT;
        } else if(diff <= TimeUnit.DAYS.toMillis(7)) {
            format = DAY_FORMAT;
        } else if(diff <= TimeUnit.DAYS.toMillis(365)) {
            format = DATE_FORMAT;
        } else {
            format = YEAR_FORMAT;
        }
        return new SimpleDateFormat(format, Locale.getDefault()).format(timestamp);
    }

}
