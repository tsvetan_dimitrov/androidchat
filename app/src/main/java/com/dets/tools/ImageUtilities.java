package com.dets.tools;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.widget.ImageView;
import android.widget.Toast;

import com.dets.base.User;
import com.dets.main.R;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Encapsulates ugly calls for images and caches images on storage.
 */
public class ImageUtilities {
    public static final String CONVERSATION_IMAGE_URL = "chat_icon2.png";

    public static void retrieveImageByUrl(ImageView imageView, String imageUrl) throws NoImageException, IOException {
        if(imageUrl == null)
            throw new NoImageException();
        String imageName = cropNameFromUrl(imageUrl);
        File imageFile = getCachedImage(imageName);
        if(imageFile.exists())
            imageView.setImageBitmap(getBitmapFromFile(imageFile));
        else
            downloadImage(imageView, imageFile, imageUrl);
    }

    private static void downloadImage(ImageView imageView, File imageFile, String imageUrl) {
        FirebaseStorage storageRef = FirebaseStorage.getInstance();
        StorageReference imageRef = storageRef.getReference().child("images/" + imageUrl);
        imageRef.getBytes(1024 * 1024 * 10)
                .addOnFailureListener(Throwable::printStackTrace)
                .addOnSuccessListener(bytes -> {
                    writeBytesToFile(imageFile, bytes);
                    Bitmap bitmap = getBitmapFromFile(imageFile);
                    imageView.setImageBitmap(bitmap);
                });
    }

    private static File getCachedImage(String imageName) throws IOException {
        File defaultDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        if(defaultDir.listFiles() == null)
            throw new IOException("Storage permission not enabled! Please give the app the Storage permission or you won't see any images.");
        for (File file : defaultDir.listFiles())
            if(imageName.equals(file.getName()))
                return file;
        return new File(defaultDir, imageName);
    }

    private static Bitmap getBitmapFromFile(File imageFile) {
        return BitmapFactory.decodeFile(imageFile.getAbsolutePath());
    }

    private static String cropNameFromUrl(String imageUrl) {
        String[] parts = imageUrl.split("[/]");
        return parts[parts.length - 1];
    }

    public static void saveImage(Context context, Bitmap image, User user) throws IOException {
        File imageFile = getCachedImage(user.getUid());
        writeImageToFile(imageFile, image);
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReference();
        Uri fileUri = Uri.fromFile(imageFile);
        StorageReference imageRef = storageRef.child("images/" + fileUri.getLastPathSegment());
        UploadTask uploadTask = imageRef.putFile(fileUri);
        uploadTask.addOnFailureListener(e ->  {
            Toast.makeText(context,
                            context.getResources().getString(R.string.error_upload_image)
                            , Toast.LENGTH_SHORT)
                    .show();
            ProgressDialogUtilities.hideProgressDialog();
            e.printStackTrace();
        });
        uploadTask.addOnSuccessListener(taskSnapshot -> {
            Toast.makeText(context,
                    context.getResources().getString(R.string.msg_upload_successful),
                    Toast.LENGTH_SHORT
            ).show();
            ProgressDialogUtilities.hideProgressDialog();
        });
        ProgressDialogUtilities.showProgressDialog(context.getString(R.string.loading), context);
    }

    @Deprecated
    @SuppressWarnings("unused")
    private static class RetrieveImageForConversationTask extends AsyncTask<Void, Void, Bitmap> {
        private WeakReference<ImageView> conversationViewReference;
        private String imageUrl;
        private File imageFile;

        private RetrieveImageForConversationTask(File imageFile, ImageView conversationView, String imageUrl) {
            this.conversationViewReference = new WeakReference<>(conversationView);
            this.imageUrl = imageUrl;
            this.imageFile = imageFile;
        }

        @Override
        protected Bitmap doInBackground(Void... voids) {
            try {
                URLConnection connection = new URL(imageUrl).openConnection();
                return BitmapFactory.decodeStream(connection.getInputStream());
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return getDefaultProfileImage(conversationViewReference.get().getContext());
            } catch (IOException e) {
                e.printStackTrace();
                return getDefaultProfileImage(conversationViewReference.get().getContext());
            }
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            writeImageToFile(imageFile, bitmap);
            conversationViewReference.get().setImageBitmap(bitmap);
        }
    }

    private static void writeImageToFile(File imageFile, Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, baos);
        byte[] imageData = baos.toByteArray();
        writeBytesToFile(imageFile, imageData);
    }

    private static void writeBytesToFile(File file, byte[] data) {
        try {
            if(!file.exists())
                file.createNewFile();
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(data);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Bitmap getDefaultProfileImage(Context context) {
        return BitmapFactory.decodeResource(context.getResources(), R.drawable.image_profile_default);
    }
}
