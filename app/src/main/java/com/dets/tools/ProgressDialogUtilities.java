package com.dets.tools;

import android.app.ProgressDialog;
import android.content.Context;

public class ProgressDialogUtilities {

    private static ProgressDialog mProgressDialog;

    public static void showProgressDialog(String text, Context context) {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage(text);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.show();
    }

    public static void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
