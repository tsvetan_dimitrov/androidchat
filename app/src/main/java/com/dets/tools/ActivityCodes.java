package com.dets.tools;

public class ActivityCodes {
    public static final int REQUEST_TAKE_PICTURE = 1;
    public static final int REQUEST_ADD_CONVERSATION = 2;
    public static final int REQUEST_ADD_USER_TO_CONVERSATION = 3;


    public static final String EXTRA_NAME_REQUEST_CODE = "requestCode";
    public static final String EXTRA_NAME_CONVERSATION_ID = "conversationId";
    public static final String EXTRA_NAME_USER_ID = "userId";
    public static final String EXTRA_NAME_CURRENT_USER_ID = "currentUserUid";
}
