package com.dets.main;

import com.dets.tools.TimeUtilities;

import org.junit.Test;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class TimeUtilitiesTest {

    @Test
    public void getFormattedTimeStamp_HoursAndMinutesParam() {
        long time = System.currentTimeMillis() - TimeUnit.MINUTES.toMillis(1439);
        String format = TimeUtilities.getFormattedTimestamp(time);
        System.out.println("Need time: " + format);
    }

    @Test
    public void getFormattedTimeStamp_LastWeekParam() {
        long time = System.currentTimeMillis() - TimeUnit.HOURS.toMillis(167);
        String format = TimeUtilities.getFormattedTimestamp(time);
        System.out.println("Need weekday: " + format);
    }

    @Test
    public void getFormattedTimeStamp_LastYearParam() {
        long time = System.currentTimeMillis() - TimeUnit.DAYS.toMillis(365);
        String format = TimeUtilities.getFormattedTimestamp(time);
        System.out.println("Need days.month: " + format);
    }

    @Test
    public void getFormattedTimeStamp_MoreThanYearParam() {
        long time = System.currentTimeMillis() - TimeUnit.DAYS.toMillis(366);
        String format = TimeUtilities.getFormattedTimestamp(time);
        System.out.println("Need date: " + format);
    }

}